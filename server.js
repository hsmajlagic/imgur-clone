const fastify = require('fastify')
const Next = require('next')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const ip = dev ? '127.0.0.1' : '0.0.0.0'
const app = Next({ dev, dir: '.' })
const handle = app.getRequestHandler()
const apiPrefix = '/api'

app.prepare()
  .then(() => {
    const server = fastify()

    // register /api/* for all Imgur API requests
    server.register(require('./routes/imgur.js'), { prefix: apiPrefix })

    // root path goes to home.js
    server.get('/', (req, res) => {
      return app.render(req.req, res.res, '/home', req.query)
    })

    // for SEO, add a parametric path mapped to Imgur Gallery API
    server.get('/gallery/:section/:sort/:window', (req, res) => {
      return app.render(req.req, res.res, '/home', { ...req.params, ...req.query })
    })

    // a dedicated route for opening gallery content
    server.get('/gallery/:id', (req, res) => {
      return app.render(req.req, res.res, '/gallery', { id: req.params.id })
    })

    // a dedicated route for opening image
    server.get('/image/:id', (req, res) => {
      return app.render(req.req, res.res, '/gallery', { id: req.params.id, origin: 'image' })
    })

    // other stuff is handled by NextJS
    server.get('/*', (req, res) => {
      return handle(req.req, res.res)
    })

    server.listen(port, ip, (err) => {
      if (err) {
        fastify.log.error(err)
        process.exit(1)
      }
      console.log(`> Ready on http://localhost:${port}`)
    })
  })
