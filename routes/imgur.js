let axios = require('axios')

let instance = axios.create({
  baseURL: 'https://api.imgur.com/3',
  headers: {
    'Authorization': 'Client-ID 95c72e85b5ade7f'
  }
})

module.exports = function (fastify, opts, next) {
  fastify.get('/*', function (request, reply) {
    // simply forward all API requests to Imgur API, passing the same raw request URL
    instance.get(`${request.raw.url.replace('/api', '')}`)
      .then(function (response) {
        reply.send(response.data)
      })
      .catch(function (error) {
        console.log(error)
        reply.send(error)
      })
  })
  next()
}
