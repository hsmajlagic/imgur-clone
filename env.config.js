const prod = process.env.NODE_ENV === 'production'

module.exports = {
  'process.env.BACKEND_URL': prod ? 'https://HEROKU_URL' : 'https://localhost:8080'
}
