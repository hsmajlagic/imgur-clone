# README #

This is small clone of Imgur website.

### Build instructions ###

* git clone https://hsmajlagic@bitbucket.org/hsmajlagic/imgur-clone.git
* npm install
* npm run dev [to start a local dev environment]
* next build && npm run prod [to start a local production environment]

developed on Node 9.5.0

### ESLint ###

* eslint --init

using standard preset

### Dependencies ###

* https://www.npmjs.com/package/next as main framework
* https://www.npmjs.com/package/fastify as thin web server and Imgur API proxy
* https://www.npmjs.com/package/axios as an isomorphic fetch client
* https://www.npmjs.com/package/lodash as a standard helper utility

### Author remarks ###

* Main entry point of the application is ./server.js where the Fastify server is initialized and run on top of NextJS. A dedicated /api route is assigned to handle and proxy requests to Imgur API. All the helper methods and configs are in ./util/api.js
* Why Fastify? Just wanted to try out something new and apparently quite fast https://www.fastify.io/benchmarks/
* The app has two pages, the home page for listing all galleries and a details page for showing various details of a selected gallery.
* Galleries can be filtered according to the available API routes, which is basically four parameters represented in the UI of home page
* By default only 20 thumbnails are shown to make the initial page load slightly faster and more responsive
* All changes to filters are immediatelly updating the state and go through the standard redux unidirectional dataflow, eventually refreshing the grid once the new API request is ready
* Web server is configured to map all filters as a URL route, which can be seen using the "Open full URL" link on the homepage - in the end just a neat SEO feature to make all API calls server-rendered as well
* Every thumbnail shows the gallery title, tags, views and comments
* Clicking the thumbnail opens the details page, with all the gallery images listed and the details of gallery shown in the header
* Every image on the details page contains a description box if a description is available

* All React components are located in the ./components folder
* Redux actions and reducers and located in the corresponding ./actions and ./reducers folders
* Redux Store is located in the root folder in store.js file
* Babel Stage 0 preset is used
* Babel parser for ESLint and React plugin for ESLint to correctly support JSX
* Testing environemnt is set up with Jest and Enzyme
* External dependencies only include Bootstrap Reboot CSS and Bootstrap Flexbox Grid, with Lato Font 

Thanks!