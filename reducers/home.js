import { UPDATE_GALLERIES, START_LOADING, STOP_LOADING } from 'actions/home.js'

export default (state, action) => {
  const { type, loading, galleries, size, section, sort, window, showViral } = action // spread all interesting properties for home reducer
  switch (type) {
  case UPDATE_GALLERIES:
    return Object.assign({}, state, {
      galleries,
      ...(section != null ? { section } : {}),
      ...(sort != null ? { sort } : {}),
      ...(window != null ? { window } : {}),
      ...(size != null ? { size } : {}),
      ...(showViral != null ? { showViral } : {})
    })
  case START_LOADING:
  case STOP_LOADING:
    return Object.assign({}, state, { loading })
  default:
    return state
  }
}
