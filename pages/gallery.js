import React from 'react'
import PropTypes from 'prop-types'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import api from 'utils/api.js'
import initStore from 'utils/store.js'
import { stopLoading } from 'actions/home.js'
import Layout from 'components/Layout.js'
import GalleryHeader from 'components/GalleryHeader.js'
import GalleryImage from 'components/GalleryImage.js'

class Gallery extends React.Component {
  static async getInitialProps ({ pathname, query: { id, origin } }) {
    let gallery = { }
    let isImage = pathname.includes('/image') || origin === 'image'
    try {
      let url = isImage ? api.routes.getImage(id) : api.routes.getGallery(id)
      let res = await api.instance.get(url)
      let data = await res.data
      gallery = data.data
    } catch (error) {
      console.log(error.response)
    }
    return {
      gallery,
      isImage
    }
  }

  static propTypes = {
    gallery: PropTypes.object.isRequired,
    isImage: PropTypes.bool,
    stopLoading: PropTypes.func
  }

  componentDidMount () {
    if (window) {
      window.onload = () => this.props.stopLoading()
    }
  }

  render () {
    let list = this.props.isImage ? [this.props.gallery] : this.props.gallery.images
    let images = list.map((item) => <GalleryImage key={item.id} type={item.type} width={item.cover_width || item.width} description={item.description} src={item.link} />)
    return (
      <Layout>
        <GalleryHeader gallery={this.props.gallery} />
        {images}
      </Layout>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    stopLoading: bindActionCreators(stopLoading, dispatch)
  }
}

export default withRedux(initStore, null, mapDispatchToProps)(Gallery)
