import React from 'react'
import PropTypes from 'prop-types'
import ScrollEvents from 'scroll-events'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import api, { options, defaults } from 'utils/api.js'
import initStore from 'utils/store.js'
import { stopLoading, receiveGalleries, fetchGalleries } from 'actions/home.js'
import Layout from 'components/Layout.js'
import HomeGrid from 'components/HomeGrid.js'
import HomeHeader from 'components/HomeHeader.js'
import HomeHeaderCheckbox from 'components/HomeHeaderCheckbox.js'

class Home extends React.Component {
  static async getInitialProps ({ store, query: { section, sort, window, showViral } }) {
    let galleries = []
    let sv = showViral != null ? showViral === 'true' : true
    try {
      let res = await api.instance.get(api.routes.getGalleries(section, sort, window, sv))
      let data = await res.data
      if (data.success) {
        galleries = data.data.slice(0, defaults.gallerySize)
      }
    } catch (error) {
      console.log(error)
    }

    // important to dispatch server-side loaded galleries into redux store along with any predefined URL parameters
    store.dispatch(receiveGalleries({ galleries, section, sort, window, showViral: sv }))

    return {
      galleries,
      size: defaults.gallerySize
    }
  }

  static propTypes = {
    galleries: PropTypes.array.isRequired,
    size: PropTypes.number.isRequired,
    defaults: PropTypes.object,
    stopLoading: PropTypes.func,
    fetchGalleries: PropTypes.func
  }

  componentDidMount () {
    if (window) {
      window.onload = () => this.props.stopLoading()
      const onTouchMoveHandler = () => {
        if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight - 50) {
          let newSize = this.props.size + 20
          if (newSize <= 60 && newSize !== this.props.size) {
            this.props.fetchGalleries({ size: newSize })
          }
        }
      }
      window.ontouchmove = onTouchMoveHandler
      new ScrollEvents().on('scroll:stop', onTouchMoveHandler)
    }
  }

  render () {
    return (
      <Layout>
        <HomeHeader sections={options.sections} sorts={options.sorts} windows={options.windows} defaults={this.props.defaults} />
        <HomeHeaderCheckbox defaults={this.props.defaults} />
        <HomeGrid images={this.props.galleries} />
      </Layout>
    )
  }
}

const mapStateToProps = ({ galleries, size, section, sort, window, showViral }) => ({ galleries, size, defaults: { section, sort, window, showViral } })

const mapDispatchToProps = (dispatch) => {
  return {
    stopLoading: bindActionCreators(stopLoading, dispatch),
    fetchGalleries: bindActionCreators(fetchGalleries, dispatch)
  }
}

export default withRedux(initStore, mapStateToProps, mapDispatchToProps)(Home)
