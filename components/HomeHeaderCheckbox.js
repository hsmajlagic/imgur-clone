import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchGalleries } from 'actions/home.js'
import Link from 'next/link'

class HomeHeaderCheckbox extends React.Component {
  static propTypes = {
    defaults: PropTypes.object.isRequired,
    fetchGalleries: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.onLabelClick = this.onLabelClick.bind(this)
    this.state = {
      showViral: props.defaults.showViral
    }
  }

  onLabelClick (e) {
    e.preventDefault()
    let showViral = !this.state.showViral
    this.setState({
      showViral
    })
    this.props.fetchGalleries({ showViral })
  }

  render () {
    let link = `/gallery/${this.props.defaults.section}/${this.props.defaults.sort}/${this.props.defaults.window}?showViral=${this.props.defaults.showViral}`
    return (
      <div className="row root">
        <div className="col">
          <input type="checkbox" id="switch" defaultChecked={this.state.showViral} /><label htmlFor="switch" onClick={this.onLabelClick}><span>Show viral</span></label>
          <Link href={link}>
            <a className="link" target="_blank">Full page reload</a>
          </Link>
        </div>
        <style jsx>{`
          .root {
            text-align: center;
            padding-left: 5px;
            padding-right: 5px;
          }
          .link {
            float: right;
          }
          input {
            height: 0;
            width: 0;
            visibility: hidden;
          }
          label {
            cursor: pointer;
            text-indent: -120px;
            width: 40px;
            height: 21px;
            background: #000;
            display: inline-block;
            border-radius: 100px;
            position: relative;
          }
          span {
            line-height: 20px;
            font-weight: bold;
            vertical-align: top;
          }
          label:after {
            content: '';
            position: absolute;
            top: 2px;
            left: 2px;
            width: 17px;
            height: 17px;
            background: #fff;
            border-radius: 17px;
            transition: 0.3s;
          }
          input:checked + label {
            background: rgb(126, 78, 153);
          }
          input:checked + label:after {
            left: calc(100% - 2px);
            transform: translateX(-100%);
          }
          label:active:after {
            width: 17px;
          }
        `}</style>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchGalleries: bindActionCreators(fetchGalleries, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(HomeHeaderCheckbox)
