import PropTypes from 'prop-types'
import timeago from 'time-ago'
import SVG from 'react-inlinesvg'

const GalleryHeader = ({ gallery }) => {
  let utcSeconds = gallery.datetime
  let d = new Date(0) // The 0 there is the key, which sets the date to the epoch
  d.setUTCSeconds(utcSeconds)
  let date = timeago.ago(d)
  return (
    <div className="root">
      <div className="title">{gallery.title}</div>
      <div className="sub-title">by <a href={`https://imgur.com/user/${gallery.account_url}`} target="_blank">{gallery.account_url}</a> {date}</div>
      <div className="row">
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-3">
          <div className="row">
            <div className="col">
              <div className="svg-container">
                <SVG src="/static/svg/arrow-circle-up.svg" style={{ fill: 'rgb(126, 78, 153)' }} />
              </div>
              <strong className="svg-label">{gallery.ups}</strong>
            </div>
            <div className="col align-center">
              <div className="svg-container">
                <SVG src="/static/svg/arrow-circle-down.svg" style={{ fill: '#aaa' }} />
              </div>
              <strong>{gallery.downs}</strong>
            </div>
            <div className="col align-right">
              <div className="svg-container">
                <SVG src="/static/svg/heart.svg" style={{ fill: '#ca6060' }} />
              </div>
              <strong>{gallery.favorite_count}</strong>
            </div>
          </div>
        </div>
        <div className="col-xs-12 col-sm-6 offset-md-4 col-md-4 offset-lg-6 col-lg-3">
          <div className="row">
            <div className="col align-center">
              <div className="svg-container">
                <SVG src="/static/svg/eye.svg" style={{ fill: '#260339' }} />
              </div>
              <strong className="svg-label">{gallery.views}</strong>
            </div>
            <div className="col align-center">
              <div className="svg-container">
                <SVG src="/static/svg/bubbles.svg" style={{ fill: '#34373c' }} />
              </div>
              <strong>{gallery.comment_count}</strong>
            </div>
          </div>
        </div>
      </div>
      <style jsx>{`
        .root {
          padding: 10px 15px 15px 15px;
          margin: 10px 0;
          background-color:#eee;
        }
        .root a {
          font-weight: bold;
        }
        .title {
          font-size: 1.4rem;
          font-weight: bold;
        }
        .sub-title {
          margin-bottom: 15px;
        }
        .align-center {
          text-align: center;
        }
        .align-right {
          text-align: right;
        }
        .svg-container {
          height: 35px;
        }
    `}</style>
    </div>
  )
}

GalleryHeader.propTypes = {
  gallery: PropTypes.object.isRequired
}

export default GalleryHeader
