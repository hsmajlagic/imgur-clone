import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import HomeMediaObjectLoader from 'components/HomeMediaObjectLoader.js'

export default class extends React.Component {
  constructor (props) {
    super(props)
    this.onLoad = this.onLoad.bind(this)
    this.state = {
      loaded: false
    }
  }

  static propTypes = {
    src: PropTypes.string.isRequired,
    alt: PropTypes.string
  }

  componentDidMount () {
    this.setState({
      src: this.props.src
    })
  }

  onLoad () {
    this.setState({
      loaded: true
    })
  }

  render () {
    return (
      <div className="root">
        <img className={classnames('image', { loaded: this.state.loaded })} src={this.state.src} alt={this.props.alt} onLoad={this.onLoad} />
        <HomeMediaObjectLoader loaded={this.state.loaded } />
        <style jsx>{`
          .root {
            position: relative;
          }
          .image {
            height: 200px;
            width: 100%;
            object-fit: cover;
            opacity: 0;
            transform: scale(0.5);
            position: relative;
            z-index: 2;
          }
          .image.loaded {
              opacity: 1;
              transform: scale(1);
              transition: all 0.4s ease-out;
          }
        `}</style>
      </div>
    )
  }
}
