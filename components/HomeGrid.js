import classnames from 'classnames'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchGalleries } from 'actions/home.js'
import HomeMediaBox from 'components/HomeMediaBox.js'

const HomeGrid = ({ images }) => {
  let imageBoxes = images.map((image) => <div key={image.id} className="col"><HomeMediaBox gallery={image} /></div>)
  return (
    <div className={classnames('row no-gutters stretch', { 'justify-content-center align-items-center': imageBoxes.length === 0 })}>
      {imageBoxes.length > 0 ? imageBoxes : <div className="col error">Oops, looks like we could not show you any galleries today. <br/>:(</div>}
      <div className="w-100"></div>
      <div className="col-12 space-bottom"></div>
      <style jsx>{`
        .stretch {
          min-height: calc(100vh - 150px);
          margin-top: 10px;
        }
        .error {
          text-align: center;
          font-size: 1.7rem;
        }
        .space-bottom {
          padding: 40px 0 50px 0;
          text-align: center;
        }
    `}</style>
    </div>
  )
}

HomeGrid.propTypes = {
  images: PropTypes.array.isRequired
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchGalleries: bindActionCreators(fetchGalleries, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(HomeGrid)
