import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchGalleries } from 'actions/home.js'
import HomeHeaderButton from 'components/HomeHeaderButton.js'

class HomeHeaderButtons extends React.Component {
  constructor (props) {
    super(props)
    this.onButtonClick = this.onButtonClick.bind(this)
    this.state = {
      selectedButton: this.props.defaults[this.props.title]
    }
  }

  static propTypes = {
    title: PropTypes.string.isRequired,
    labels: PropTypes.array.isRequired,
    defaults: PropTypes.object.isRequired,
    fetchGalleries: PropTypes.func
  }

  onButtonClick (label) {
    this.setState({
      selectedButton: label
    })
    let title = this.props.title
    this.props.fetchGalleries({ [title]: label })
  }

  render () {
    let selectedButton = this.state.selectedButton
    let btnGroup = this.props.labels.map((label, index) => <div key={index} className="col"><HomeHeaderButton onClick={this.onButtonClick} isSelected={selectedButton === label} label={label} title={this.props.title} /></div>)
    return (
      <div className="row no-gutters root">
        <div className="col-lg-12 title">{this.props.title}</div>
        {btnGroup}
        <style jsx>{`
          .root {
            background-color: #eee;
            text-align: center;
            padding: 5px 10px 10px 10px;
          }
          .title {
            text-transform: capitalize;
            font-size: 1rem;
            font-weight: bold;
            margin-bottom: 5px;
          }
        `}</style>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchGalleries: bindActionCreators(fetchGalleries, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(HomeHeaderButtons)
