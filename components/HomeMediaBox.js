import _some from 'lodash/some'
import _find from 'lodash/find'
import Link from 'next/link'
import PropTypes from 'prop-types'
import HomeMediaObject from 'components/HomeMediaObject.js';

const HomeMediaBox = ({ gallery }) => {
  let url = gallery.is_album ? 'gallery' : 'image'
  let isVideo = Boolean((gallery.type && gallery.type.includes('video')) || (gallery.images && gallery.images.length > 0 && _some(gallery.images, (item) => item.type.includes('video'))))
  let src, alt, description
  if (isVideo) {
    let data = _find(gallery.images, (item) => item.type.includes('video'))
    src = gallery.mp4 ? gallery.mp4 : data.mp4
    description = (data && data.description) || gallery.title
  } else {
    let img = gallery.images && gallery.images.length > 0 ? _find(gallery.images, (item) => !item.type.includes('video')) : null
    src = img ? img.link : gallery.link
    alt = img ? img.title : ''
    description = (img && img.description) || gallery.title
  }
  let tags = gallery.tags && gallery.tags.length > 0 && gallery.tags.map((item, index) => <span key={index} style={{ display: 'inline-block', border: '0.5px solid #bbb', borderRadius: '5px', margin: '5px 2px 0 2px', padding: '2px 4px' }}>{item.display_name}</span>)
  return (
    <div className="root">
      <Link href={`/${url}/${gallery.id}`}>
        <a>
          <HomeMediaObject isVideo={isVideo} src={src} alt={alt} />
        </a>
      </Link>
      <div className="details">
        {description.length > 150 ? `${description.slice(0, 150)}...` : description }
        <div className="views">» {gallery.views} views <span>{gallery.comment_count} comments «</span></div>
        <div className="tags">{tags}</div>
      </div>
      <style jsx>{`
        .root {
          padding: 0;
          border: 5px solid #fff;
          min-width: 200px;
          width: 100%;
          transition: all 0.2s linear;
          position: relative;
          background-color: transparent;
          z-index: 2;
        }
        .root:hover {
          transform: scale(1.1);
          z-index: 3;
        }
        .details {
          box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
          position: absolute;
          z-index: 4;
          left: 0;
          right: 0;
          top: calc(100% + 1px);
          padding: 5px 10px 8px 10px;
          background-color: #000;
          border: 5px solid #34373C;
          color: #fff;
          font-size: 0.85rem;
          line-height: 1.1rem;
          opacity: 0;
          white-space: pre-line;
          word-wrap: break-word;
          max-height: 0;
          overflow: hidden;
        }
        .root:hover > .details {
            opacity: 1;
            max-height: 200px;
            transition: all 0.3s ease-out;
        }
        .views {
          border-top: 1px solid #34373C;
          text-align: left;
          margin-top: 7px;
          padding-top: 5px;
          color: #aaa;
          font-size: 0.8rem;
        }
        .views > span {
          float: right;
        }
        .tags {
          color: #aaa;
          margin-top: 3px;
          margin-bottom: 2px;
          word-wrap: break-word;
        }
    `}</style>
    </div>
  )
}

HomeMediaBox.propTypes = {
  gallery: PropTypes.object.isRequired
}

export default HomeMediaBox
