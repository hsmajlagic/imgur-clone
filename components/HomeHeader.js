import HomeHeaderButtons from 'components/HomeHeaderButtons.js'
import { PropTypes } from 'prop-types';

const HomeHeader = ({ defaults, sections = [], sorts = [], windows = [] }) => {
  return (
    <div className="row root">
      <div className="col-xs-12 col-sm-4"><HomeHeaderButtons labels={sections} title="section" defaults={defaults} /></div>
      <div className="col-xs-12 col-sm-4"><HomeHeaderButtons labels={sorts} title="sort" defaults={defaults} /></div>
      <div className="col-xs-12 col-sm-4"><HomeHeaderButtons labels={windows} title="window" defaults={defaults} /></div>
      <style jsx>{`
        .root {
          margin-top: 20px;
          margin-bottom: 20px;
          padding-left: 5px;
          padding-right: 5px;
        }
        .button {
          background-color: #543D7A;
        }
    `}</style>
    </div>
  )
}

HomeHeader.propTypes = {
  defaults: PropTypes.object.isRequired,
  sections: PropTypes.array,
  sorts: PropTypes.array,
  windows: PropTypes.array
}

export default HomeHeader
