import classnames from 'classnames'
import PropTypes from 'prop-types'

const HomeMediaObjectLoader = ({ loaded = false }) => (
  <div className={classnames('root', { loaded })}>
    <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
    <style jsx>{`
      .root {
        width: 60px;
        height: 60px;
        position: absolute;
        top: 50%;
        left: 0;
        right: 0;
        margin: 0 auto;
        transform: translateY(-50%);
        z-index: 3;
      }
      .root.loaded {
        display: none;
      }
      @keyframes ball-grid-pulse {
        0% {
            transform: scale(1); }
        50% {
            transform: scale(0.5);
          opacity: 0.7; }
        100% {
              transform: scale(1);
          opacity: 1; }
      }
      .root > div:nth-child(1) {
                animation-delay: 0.22s;
                animation-duration: 0.9s; }
      .root > div:nth-child(2) {
                animation-delay: 0.64s;
                animation-duration: 1s; }
      .root > div:nth-child(3) {
                animation-delay: -0.15s;
                animation-duration: 0.63s; }
      .root > div:nth-child(4) {
                animation-delay: -0.03s;
                animation-duration: 1.24s; }
      .root > div:nth-child(5) {
                animation-delay: 0.08s;
                animation-duration: 1.37s; }
      .root > div:nth-child(6) {
                animation-delay: 0.43s;
                animation-duration: 1.55s; }
      .root > div:nth-child(7) {
                animation-delay: 0.05s;
                animation-duration: 0.7s; }
      .root > div:nth-child(8) {
                animation-delay: 0.05s;
                animation-duration: 0.97s; }
      .root > div:nth-child(9) {
                animation-delay: 0.3s;
                animation-duration: 0.63s; }
      .root> div {
        background-color: #ddd;
        width: 15px;
        height: 15px;
        border-radius: 100%;
        margin: 2px;
        animation-fill-mode: both;
        display: inline-block;
        animation-name: ball-grid-pulse;
        animation-iteration-count: infinite;
        animation-delay: 0; }
      `}</style>
  </div>
)

HomeMediaObjectLoader.propTypes = {
  loaded: PropTypes.bool
}

export default HomeMediaObjectLoader
