import PropTypes from 'prop-types'

const GalleryImage = ({ src, description, type, width = '100%' }) => {
  let isVideo = Boolean(type && type.includes('video'))
  let httpsSrc = src.includes('https') ? src : src.replace('http', 'https')
  return (
    <div className="root">
      <div className="image-container">
        {isVideo ? <video autoPlay loop src={httpsSrc} /> : <img src={httpsSrc} style={{ maxWidth: '100%' }} />}
        {description ? <div className="description">{description}</div> : null}
      </div>
      <hr />
      <style jsx>{`
        .root {
          text-align: center;
          margin-bottom: 15px;
        }
        .image-container {
          max-width: 100%;
          overflow: hidden;
          width: auto;
          margin: 0 auto;
          display: inline-block;
          position: relative;
          z-index: 2;
        }
        .description {
          max-width: ${width}px;
          white-space: pre-line;
          word-break: break-all;
          padding: 10px 15px 10px 15px;
          background-color: #000;
          border: 5px solid #34373C;
          color: #fff;
          font-size: 1.1rem;
          line-height: 1.5rem;
          margin-top: -5px;
          text-align: left;
          position: relative;
          z-index: 3;
        }
    `}</style>
    </div>
  )
}

GalleryImage.propTypes = {
  src: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  description: PropTypes.string,
  width: PropTypes.string
}

export default GalleryImage
