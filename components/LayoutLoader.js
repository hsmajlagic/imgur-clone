import classnames from 'classnames'
import { connect } from 'react-redux'
import PropTypes from 'prop-types';

const LayoutLoader = ({ loading = false }) => {
  return (
    <div className={classnames('root', { 'visible': loading })}>
      <div className='loader'>
        <div></div><div></div><div></div>
      </div>
      <style jsx>{`
        .root {
            z-index: 2147483647;
            top: 4px;
            right: 5px;
            position: fixed;
            display: none;
            pointer-events: none;
        }
        .root.visible {
            display: block;
        }
        .loader {
            position: relative;
            width: 45px;
            height: 45px;
        }
        .loader > div {
            background-color: rgb(139, 101, 161);
            border-radius: 100%;
            position: absolute;
            left: 0;
            top: 0;
            opacity: 0;
            margin: 0;
            width: 45px;
            height: 45px;
            -webkit-animation: animate-loader 1s 0s linear infinite;
            animation: animate-loader 1s 0s linear infinite;
        }
        .loader > div:nth-child(2) {
            animation-delay: -.4s;
        }
        .loader > div:nth-child(3) {
            animation-delay: -.2s;
        }
        @keyframes animate-loader {
            0% {
                -webkit-transform: scale(0);
                        transform: scale(0);
                opacity: 0; }
            5% {
                opacity: 1; }
            100% {
                -webkit-transform: scale(1);
                        transform: scale(1);
                opacity: 0; }
        }
      `}</style>
    </div>
  )
}

LayoutLoader.propTypes = {
  loading: PropTypes.bool
}

const mapStateToProps = ({ loading }) => ({ loading })

export default connect(mapStateToProps, null)(LayoutLoader)
