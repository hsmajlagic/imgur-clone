import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import HomeMediaObjectLoader from 'components/HomeMediaObjectLoader.js'

export default class extends React.Component {
  constructor (props) {
    super(props)
    this.onLoad = this.onLoad.bind(this)
    this.state = {
      loaded: false
    }
  }

  static propTypes = {
    src: PropTypes.string.isRequired
  }

  componentDidMount () {
    this.setState({
      src: this.props.src
    })
  }

  onLoad () {
    this.setState({
      loaded: true
    })
  }

  render () {
    return (
      <div>
        <video autoPlay loop className={classnames('video', { loaded: this.state.loaded })} src={this.state.src} onLoadedData={this.onLoad} />
        <HomeMediaObjectLoader loaded={this.state.loaded } />
        <style jsx>{`
          .video {
            height: 200px;
            width: 100%;
            object-fit: cover;
            object-position: 50% 0;
            opacity: 0;
            transform: scale(0.5);
          }
          .video.loaded {
              opacity: 1;
              transform: scale(1);
              transition: all 0.4s ease-out;
          }
        `}</style>
      </div>
    )
  }
}
