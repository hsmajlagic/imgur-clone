import Head from 'next/head'
import LayoutLoader from 'components/LayoutLoader.js'
import { PropTypes } from 'prop-types';

const prod = process.env.NODE_ENV === 'production'
const bootstrapRebootCssUrl = prod ? 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap-reboot.min.css' : 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap-reboot.css'
const bootstrapGridCssUrl = prod ? 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap-grid.min.css' : 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap-grid.css'

const Layout = ({ children, title = 'Home' }) => (
  <div>
    <Head>
      <title>{ `MobiLab Imgur - ${title}` }</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,700" />
      <link rel="stylesheet" href={bootstrapRebootCssUrl} />
      <link rel="stylesheet" href={bootstrapGridCssUrl} />
      <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-32x32.png" sizes="32x32" />
      <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-96x96.png" sizes="96x96" />
      <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-16x16.png" sizes="16x16" />
    </Head>
    <noscript></noscript>
    <header>
      <h1 className="header">
        <a href="/">The Most Viral Images on the Internet</a>
      </h1>
    </header>
    <div className="container">
      <LayoutLoader />
      { children }
    </div>
    <footer />
    <style jsx global>{`
      html {
        font-size: 14px;
        font-family: 'Lato', sans-serif;
      }
      body {
        background-color: #fff;
      }
      header {
        background-color: #34373c;
        text-align: center;
        padding: 10px 20px 15px 20px;
      }
      h1.header {
        font-size: 1.4rem;
        font-weight: normal;
        margin: 0;
      }
      h1.header a {
        text-decoration: none;
        color: #fff;
        display: block;
      }
      h1.header:hover a {
        text-decoration: underline;
      }
    `}</style>
  </div>
)

Layout.propTypes = {
  children: PropTypes.any,
  title: PropTypes.string
}

export default Layout
