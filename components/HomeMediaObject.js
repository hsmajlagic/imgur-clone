import PropTypes from 'prop-types'
import HomeMediaImage from 'components/HomeMediaImage.js'
import HomeMediaVideo from 'components/HomeMediaVideo.js'

const HomeMediaObject = ({ isVideo, src }) => {
  let httpsSrc = src.includes('https') ? src : src.replace('http', 'https')
  return (
    <div className="root">
      {isVideo ? <HomeMediaVideo src={httpsSrc} /> : <HomeMediaImage src={httpsSrc} /> }
      <style jsx>{`
        .root {
          cursor: pointer;
          position: relative;
          transition: box-shadow 0.4s ease-out;
          height: 200px;
          overflow: hidden;
        }
        .root:hover {
          box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        }
    `}</style>
    </div>
  )
}

HomeMediaObject.propTypes = {
  isVideo: PropTypes.bool.isRequired,
  src: PropTypes.string.isRequired
}

export default HomeMediaObject
