import classnames from 'classnames'
import { PropTypes } from 'prop-types';

const HomeHeaderButton = ({ onClick, isSelected, label, title }) => {
  let color = title === 'section' ? '#260339' : (title === 'sort' ? '#3D1255' : '#582A72')
  return (
    <button className={classnames('root', { selected: isSelected })} onClick={() => onClick(label)}>
      {label}
      <style jsx>{`
        .root {
          width: 100%;
          padding: 5px 5px 7px 5px;
          font-size: 0.9rem;
          border: none;
          outline: none;
        }
        .root:hover {
          cursor: pointer;
          background-color: #ccc;
        }
        .root.selected {
          background-color: ${color};
          color: #fff;
        }
      `}</style>
    </button>
  )
}

HomeHeaderButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  isSelected: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
}

export default HomeHeaderButton
