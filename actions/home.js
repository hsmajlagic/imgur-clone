import api from 'utils/api.js'

export const START_LOADING = 'START_LOADING'
export const STOP_LOADING = 'STOP_LOADING'
export const UPDATE_GALLERIES = 'UPDATE_GALLERIES'

export const startLoading = () => {
  return { type: START_LOADING, loading: true }
}

export const stopLoading = () => {
  return { type: STOP_LOADING, loading: false }
}

export const receiveGalleries = (params) => dispatch => {
  return dispatch({ type: UPDATE_GALLERIES, ...params })
}

export const fetchGalleries = ({section, sort, window, showViral, size}) => {
  return async (dispatch, getState) => {
    dispatch(startLoading())

    // read out the current state if parameters are omitted
    let _section = section || getState().section
    let _sort = sort || getState().sort
    let _window = window || getState().window
    let _showViral = showViral != null ? showViral : getState().showViral
    let _size = size || getState().size

    const onSuccess = (res) => {
      let data = res.data
      let galleries = data.success ? data.data.slice(0, _size) : []
      dispatch(receiveGalleries({ galleries, size: _size, section: _section, sort: _sort, window: _window, showViral: _showViral }))
      dispatch(stopLoading())
      return res
    }
    const onError = (res) => {
      dispatch(stopLoading())
      return res
    }
    try {
      let res = await api.instance.get(api.routes.getGalleries(_section, _sort, _window, _showViral))
      return onSuccess(res)
    } catch (error) {
      return onError(error)
    }
  }
}
