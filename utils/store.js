import { createStore, compose, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import homeReducer from 'reducers/home.js'
import { options, defaults } from 'utils/api.js'

// sensible defaults for initial state
const initialState = {
  galleries: [], // empty list of galleries before fetching from API
  loading: true, // show the spinner on initial load
  section: options.sections[defaults.section],
  sort: options.sorts[defaults.sort],
  window: options.windows[defaults.window],
  showViral: defaults.showViral,
  size: defaults.gallerySize
}

export default (state = initialState) => {
  return createStore(homeReducer, state, compose(applyMiddleware(thunkMiddleware))) // thunk middleware for API calls in actions
}
