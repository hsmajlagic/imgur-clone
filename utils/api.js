import axios from 'axios'

export const baseURL = process.env.NODE_ENV !== 'production' ? 'http://localhost:3000' : 'https://haris-imgur.herokuapp.com'

const api = {
  instance: axios.create({
    baseURL
  }),
  routes: {
    getGalleries: (section = 'hot', sort = 'viral', window = 'day', showViral = true) => {
      return `/api/gallery/${section}/${sort}/${window}/?showViral=${showViral}`
    },
    getGallery: (id = '0') => {
      return `/api/gallery/album/${id}`
    },
    getImage: (id = '0') => {
      return `/api/gallery/image/${id}`
    }
  }
}

export const options = {
  sections: ['hot', 'top', 'user'],
  sorts: ['viral', 'top', 'time', 'rising'],
  windows: ['day', 'week', 'month', 'year', 'all']
}

export const defaults = {
  section: 0,
  sort: 0,
  window: 0,
  showViral: true,
  gallerySize: 20
}

export default api
